# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 08:33:23 2019

@author: Jessie Lu
"""
from sqlalchemy import create_engine
import traceback 
import sys  # for writing detail error message
from errorLogger import errorLogger
from connect_info import mysql_info, mssql_info

def ConnectionMySql(logger):
    try:
        engine = create_engine('mysql+pymysql://{0}:{1}@{2}/{3}?charset=utf8'.format(mysql_info['user'],
                                                                                   mysql_info['password'],
                                                                                   mysql_info['address']
                                                            encoding='utf8', connect_args={'charset':'utf8'})
        return engine
    except Exception as err:
        errorLogger(err, logger)

def ConnectionSQLServer(logger):
        try:
            sqlEngine = create_engine('mssql+pymssql://{0}:{1}@{2}/{3}?charset=utf8'.format(
                                                                            mssql_info['user'],
                                                                            mssql_info['password'],
                                                                            mssql_info['address'],
                                                                            mssql_info['database']))
            return sqlEngine                                                                
        except Exception as err:
            errorLogger(err, logger)
                               
mysql_info = {
    #'address': '52.199.201.241',
    'address': '18.182.4.46',
    'user': 'abccar',
    'password': 'abccar'
}
'''
mssql_info = {
    'address': 'pddevelop.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com',
    'user': 'administrator',
    'password': '!abccar2020',
    'database': 'ABCCarInfo'
}
'''
mssql_info = {
    'address': 'pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com',
    'user': 'administrator',
    'password': '!abccar2020',
    'database': 'ABCCarInfo'
}

