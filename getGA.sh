export PATH=$PATH:/home/hadoop/haa_recommend/

set -e # terminate if error occurred
echo GetGA.py starts to run!
time (/home/hadoop/anaconda3/bin/python '/home/hadoop/haa_recommend/GetGA.py')
echo GetGA.py finished!
