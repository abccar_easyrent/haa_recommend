# -*- coding: utf-8 -*-
import smtplib
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email.utils import COMMASPACE

#  test
# global variables
b1day = (datetime.today()).strftime('%Y-%m-%d')
group_receiver_ls = ["MARH@hotaimotor.com.tw","JACK3@hotaimotor.com.tw","31879@hotaimotor.com.tw","TOM7330@hotaimotor.com.tw","SAM903@hotaimotor.com.tw","HFC30318@hotaimotor.com.tw","31970@hotaimotor.com.tw","maggiechen@hotaimotor.com.tw","TWEETY@hotaimotor.com.tw","RONG0105@mail.hotaimotor.com.tw","LSTTR12@hotaimotor.com.tw","JOANNE0122@hotaimotor.com.tw","YIYENCHEN@hotaimotor.com.tw","30632@hotaimotor.com.tw","OOKACHAKA@hotaimotor.com.tw","RUDYJIN@hotaimotor.com.tw","JOANNE373@hotaimotor.com.tw","MARS@hotaimotor.com.tw","WILLIAM@hotaimotor.com.tw","STEVEWU@hotaimotor.com.tw","KAYSHUNSUN@hotaimotor.com.tw","ANDREW@hotaimotor.com.tw","MANDYLIN@hotaimotor.com.tw","JESSIELU@hotaimotor.com.tw","EDDIELIN@hotaimotor.com.tw","MARKYAO@hotaimotor.com.tw"]
#group_receiver_ls = ["JESSIELU@hotaimotor.com.tw"]
private_receiver_ls = ["JESSIELU@hotaimotor.com.tw"]
cc = ["YSLIU@hotaimotor.com.tw", "FRED@hotaimotor.com.tw", "CARLO@hotaimotor.com.tw", "AMANDACHEN@hotaimotor.com.tw", "YIHUA23@hotaimotor.com.tw"]
sender_ls = "abccar.easyrent@gmail.com"


def smtp_connect(sender, receivers_list, msg):
    try:
        # Gmail Sign In
        gmail_sender = sender_ls
        gmail_passwd = 'hffeqpmbsftiwmyh'  # 要至官網申請應用程式密碼

        smtp = smtplib.SMTP('smtp.gmail.com:587')
        smtp.ehlo()
        smtp.starttls()
        smtp.login(gmail_sender, gmail_passwd)
        smtp.sendmail(sender, receivers_list, msg.as_string())
        smtp.quit()
        print('send successfully')
    except Exception as e:
        print('Not sent email because %s' % e)

# 日報：Email寄送報告
def send_daily_report(receiver, path):

    sender = sender_ls
    msg = MIMEMultipart()
    msg['From'] = sender

    if receiver == 'g':
        receivers_list = group_receiver_ls
        msg['Cc'] = COMMASPACE.join(cc)

    elif receiver == 'j':
        receivers_list = private_receiver_ls
        # msg['Cc'] = COMMASPACE.join(receivers_list)

    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = 'abc好車網 營運日報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    abc好車網 營運日報。
    若有疑問請來信: jessielu@hotaimotor.com.tw
    謝謝!
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # instance of MIMEApplication and named as p
    filename = "sales_update.xlsx"
    p = MIMEApplication(open(path+"sales_update.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # the image directory
    fp = open(path+'sales_graph.png', 'rb')
    msgImage = MIMEImage(fp.read())
    msgImage.add_header('Content-ID', 'sales_graph', filename='sales_graph')

    # attach the instance 'p' and 'msgImage' to instance 'msg'
    msg.attach(p)
    msg.attach(msgImage)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# 日報：Email寄送出錯訊息
def send_daily_report_error(message):

    sender = sender_ls
    receivers_list = ["jessielu@hotaimotor.com.tw"]

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '日報出錯訊息_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    日報程式排程錯誤訊息。主要錯誤發生段落:{}
    """.format(message)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# 虛擬號碼日報：Email寄送虛擬號碼日報
def send_vc_report(receiver, path):

    sender = sender_ls
    if receiver == 'g':
        receivers_list = group_receiver_ls
    elif receiver == 'j':
        receivers_list = private_receiver_ls


    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '虛擬號碼來電數日報_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    虛擬號碼日報。[備註:1.刪除內部的測試帳號.2.會員身份固定以每日早上9點狀態為主. 
    3.成功接聽數:通話10秒內+通話5分鐘內+通話5-10分鐘內+通話超過10分鐘.
    4.接聽率:成功接聽數/被撥打數。]"
    """
    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # open the file to be sent
    filename = "虛擬號碼來電數日報.xlsx"

    # instance of MIMEBase and named as p
    p = MIMEApplication(open(path+"虛擬號碼來電數日報.xlsx", "rb").read())
    p.add_header('Content-Disposition', "attachment", filename = filename)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# 虛擬號碼日報：Email寄送出錯訊息
def send_vc_error(message):

    sender = sender_ls
    receivers_list = private_receiver_ls

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '虛擬號碼日報出錯訊息_{0}'.format(b1day)

    # string to store the body of the mail
    body = """
    虛擬號碼日報排程錯誤訊息。主要錯誤發生段落:{}
    """.format(message)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)


# 針對非日報, 處理資料中發生錯誤寄出錯誤訊息
def send_error_message(script, message):

    sender = sender_ls
    # 固定寄給自己
    receivers_list = private_receiver_ls

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers_list)
    msg['Subject'] = '執行 {} 程式時發生的錯誤訊息'.format(script)

    # string to store the body of the mail
    body = """
    當在執行 {0} 程式時出現錯誤訊息。主要錯誤發生段落:{1}
    """.format(script, message)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # log in smtp and send out email
    smtp_connect(sender, receivers_list, msg)

