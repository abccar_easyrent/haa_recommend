import pandas as pd
import sqlalchemy as sa

### READ IN DATABASE ###
# AWS MySQL
def read_mysql(query, db_name):

    engineX = 'mysql+pymysql://abccar:abccar@18.182.4.46:3306/{0}?charset=utf8'.format(db_name)
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset':'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data

# Local DB
def read_mydb(query, db_name):

    engineX = 'mysql+pymysql://root:abccar@172.16.221.143:3306/{0}?charset=utf8'.format(db_name)
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset': 'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data

# NICK AWS DB
def read_testdb(query, db_name):

    engineX = 'mysql+pymysql://abccar:abccar@54.168.246.58:3306/{0}?charset=utf8'.format(db_name)
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset':'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data

# BILLY DB
def read_billy_db(query, db_name):

    engineX = 'mysql+pymysql://root:111@172.16.221.133:3306/{0}?charset=utf8'.format(db_name)
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset': 'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data

# SQL SERVER MASTER DB
def read_mssql(query, db_name):

    user = 'administrator'
    password = '!abccar2020'
    address = 'pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'

    engineX = f'mssql+pymssql://{user}:{password}@{address}/{db_name}?charset=utf8'
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset':'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data

# SQL SERVER TEST(DEVELOPMENT) DB
def read_test_mssql(query, db_name):

    user = 'administrator'
    password = '!abccar2020'
    address = 'pddevelop.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'

    engineX = f'mssql+pymssql://{user}:{password}@{address}/{db_name}?charset=utf8'
    engine = sa.create_engine(engineX, encoding='utf8', connect_args={'charset':'utf8'})
    get_data = pd.read_sql(query, con=engine)

    return get_data


### WRITE DATAFRAME TO DATABASE
# LOCAL DB
def to_mydb(db_name, data, db_tableName, method):

    engine = sa.create_engine('mysql+pymysql://root:abccar@172.16.221.143:3306/{0}?charset=utf8'.format(db_name))
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists = method, index = False) #method = 'replace' or 'append'

# NICK AWS DB
def to_testdb(db_name, data, db_tableName, method):

    engine = sa.create_engine('mysql+pymysql://abccar:abccar@54.168.246.58:3306/{0}?charset=utf8'.format(db_name))
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists = method, index = False)

# BILLY DB
def to_billy_db(db_name, data, db_tableName, method):

    engine = sa.create_engine('mysql+pymysql://root:111@172.16.221.133:3306/{0}?charset=utf8'.format(db_name))
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists = method, index = False) #method = 'replace' or 'append'

# AWS MySQL
def to_mysql(db_name, data, db_tableName, method):

    engine = sa.create_engine('mysql+pymysql://abccar:abccar@18.182.4.46:3306/{0}?charset=utf8mb4'.format(db_name))
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists = method, index = False) #method = 'replace' or 'append'

# SQL SERVER MASTER DB
def to_sql_server(db_name, data, db_tableName, method):

    user = 'administrator'
    password = '!abccar2020'
    address = 'pdmasterdb.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'

    engine = sa.create_engine(f'mssql+pymssql://{user}:{password}@{address}/{db_name}?charset=utf8')
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists= method, index=False) #method = 'replace' or 'append'


# SQL SERVER TEST(DEVELOPMENT) DB
def to_test_sql_server(db_name, data, db_tableName, method):

    user = 'administrator'
    password = '!abccar2020'
    address = 'pddevelop.cpambwr2r6oj.ap-northeast-1.rds.amazonaws.com'

    engine = sa.create_engine(f'mssql+pymssql://{user}:{password}@{address}/{db_name}?charset=utf8')
    data.to_sql('{0}'.format(db_tableName), con=engine, if_exists= method, index=False) #method = 'replace' or 'append'


