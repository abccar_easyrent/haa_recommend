# -*- coding: utf-8 -*-
import sys
#sys.path.insert(0, '/home/jessie/MyNotebook/')
sys.path.insert(0, '/home/hadoop/haa_recommend/')
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
import re
import pandas as pd
import sqlalchemy as sa
import os
from datetime import datetime, timedelta, date

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
os.chdir('/home/hadoop/haa_recommend')
KEY_FILE_LOCATION = 'gaApiKey.json'
VIEW_ID = '146219892'

def initialize_analyticsreporting():
    """Initializes an Analytics Reporting API V4 service object.

    Returns:
        An authorized Analytics Reporting API V4 service object.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    return analytics

def get_report(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
        analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
        The Analytics Reporting API V4 response.
    """

    reports = []
    pageCnt = 1
    pageToken = None
    while True:
        data = analytics.reports().batchGet(
            body={
                'reportRequests': [{
                    'viewId': VIEW_ID,
                    'pageToken': pageToken,
                    'samplingLevel': 'LARGE',
                    'dateRanges': [{'startDate':'yesterday', 'endDate': 'yesterday'}],
                    'metrics': [{'expression': 'ga:totalEvents'}],
                    'dimensions': [
                        {'name': 'ga:date'},
                        {'name': 'ga:dimension3'},
                        {'name': 'ga:eventCategory'},
                        {'name': 'ga:eventAction'},
                        {'name': 'ga:eventLabel'},
                        {'name': 'ga:dimension2'}
                    ],
                    'dimensionFilterClauses': [
                        {
                            'filters': [{
                                'dimensionName': 'ga:eventAction',
                                'operator': 'IN_LIST',
                                'expressions':  
                                ["照片點擊", "點檢表點擊", "監理資料點擊", "行情趨勢點擊"]
                            }]
                        },
                        {
                          "filters": [{
                              "dimensionName": "ga:eventCategory",
                              "operator": "IN_LIST",
                              "expressions": 
                              ["拍賣車輛資訊查詢", "拍賣結果查詢"]
                            }]
                        }
                    ]
                }]
            }
        ).execute()
        reports.append(data)
        print('Get Page %i of data.' % (pageCnt))
        pageCnt += 1
        if 'nextPageToken' in data['reports'][0]:
            pageToken = data['reports'][0]['nextPageToken']
        else:
            break
  
    return reports

def prep_data(rawData):
#     print(rawData)
    rows = []
    for data in rawData:
        data = data['reports'][0]
        
        cols = data['columnHeader']
        cols = cols['dimensions'] + [cols['metricHeader']['metricHeaderEntries'][0]['name']]
        cols = [re.sub('ga:','',x) for x in cols]
    
        data = data['data']['rows']
    
        for row in data:
            date = row['dimensions'][0]
            date = '-'.join([date[:4], date[4:6], date[6:]])
            row['dimensions'][0] = date
            rows.append(row['dimensions']+row['metrics'][0]['values'])

#     cols[4] = 'CarID'
#     print(cols)
    final = pd.DataFrame(rows, columns=cols)
    
    deleteList = []
    for i,v in final.iterrows():
        if(len(v['eventLabel']) != 26):
            deleteList.append(i)
    filterFinal = final[~final.index.isin(deleteList)]
    filterFinal.rename(columns={'dimension3':'memberID', 'dimension2': 'eventTime'}, inplace=True)    
    
    return filterFinal    

def insert_mysql(data):
    engine = sa.create_engine('mysql+pymysql://abccar:abccar@18.182.4.46:3306/HAA?charset=utf8', encoding='utf8')
    data.to_sql('ga_event',con=engine,if_exists='append', index=False)
    

def main():

    script = 'GetGA.py'
    now = date.today()
    print(now)
    
    try:
        analytics = initialize_analyticsreporting()
    except Exception as e:
        msg = 'PART 1: 連線GA API 讀取失敗! The error message : {}'.format(e)
#         send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
        
    try:
        response = get_report(analytics)
    except Exception as e:
        count = 0
        msg = 'PART 2: Fetch GA 資料出現錯誤! The error message : {}'.format(e)
#         send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 2: Finished!')

    try:
        final = prep_data(response)
    except Exception as e:
        msg = 'PART 3: 清整GA資料出現錯誤! The error message : {}'.format(e)
#         send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)

    print('PART 3: Finished!')

    try:
        insert_mysql(final)
    except Exception as e:
        msg = 'PART 4: 寫入資料庫出現錯誤! The error message : {}'.format(e)
#         send_error_message(script=script, message=msg)
        print(msg)
        sys.exit(1)
    print('PART 4: Finished!')
        
if __name__ == '__main__':
    main()
