# -*- coding: utf-8 -*-
"""
Created on 2020/11/10

@author: JessieLu
"""

# pip3 install sqlalchemy, mysqlclient
import pandas as pd
import numpy as np
import pymysql.cursors  
import pymysql
from pandas.io import sql
from sqlalchemy import create_engine
import sqlalchemy 
import logging
import sys  # for writing detail error message
import os 
import time
import warnings
import traceback 
from pandas.core.common import SettingWithCopyWarning
from datetime import datetime
from errorLogger import errorLogger
from Module.ConnFunc import read_mysql
from Module.HelpFunc import display_set
from Module.EmailFunc import send_daily_report, send_daily_report_error
from datetime import datetime, timedelta, date

def readDataInsideDB():
    # 選取範圍欄位名稱
    df = pd.read_excel("拍賣資料_20210101-0430.xlsx",
                         sheet_name="會員")
    # 選取範圍欄位名稱
    df_car = pd.read_excel("拍賣資料_20210101-0430.xlsx",
                         sheet_name="車輛",converters={'CARNO':str})
    # 選取範圍欄位名稱
    df_auction = pd.read_excel("拍賣資料_20210101-0430.xlsx",
                         sheet_name="拍賣場次",converters={'BSELL_DATE_NO':str,'S_NO':str, 'CARNO':str, 'YM_MFG':str, '成交價OR最後出價':str})
    
    return df, df_car, df_auction

def readDataDB():
    query1 = " (SELECT * FROM HAA.ga_event where TO_DAYS(NOW()) - TO_DAYS(eventTime) <=60);"
    getGA_least30Days = read_mysql(query1, 'HAA')
    getGA_least30Days[['AuctionDate', 'Number']] = getGA_least30Days['eventLabel'].str.split(",",expand=True)
    getGA_least30Days['filterAuctionDate'] = getGA_least30Days['AuctionDate'].str.split(':',expand=True)[1].str.replace('/', '').str.strip()
    getGA_least30Days['filterNumber'] = getGA_least30Days['Number'].str.split(':', expand=True)[1].str.strip()
    
    return getGA_least30Days

def mergeData(getGA_least30Days, df, df_auction):
    event_auction = pd.merge(getGA_least30Days, df_auction, left_on=['filterAuctionDate', 'filterNumber'], right_on=['BSELL_DATE_NO', 'S_NO'], how='inner')
    member_event_auction = pd.merge(df, event_auction, left_on=['BUYERID'], right_on=['memberID'], how='left')
    member_event_auction = member_event_auction.dropna()

    return member_event_auction

def categoryTag(i):
    if i == 7:
        return '法拍車'
    elif i == 1:
        return '高價進口車'
    else:
        return '其他'

def conditionTag(i):
    if i =='Y':
        return '可認證車'
    else:
        return '不可認證車'

def yearTag(i):
    now = str(date.today().strftime("%Y.%m"))
    if(float(now) - float(i)) < 4:
        return '4年內'
    elif (float(now) - float(i)) > 4 and (float(now)- float(i)) < 8:
        return '5-8年'
    else:
        return '8年以上'
    
def mileTag(i):
    if i =='Y':
        return '里程保證'
    else:
        return '里程不保證'
    
def priceTag(i):
    if float(i) < 200000:
        return '20萬內'
    elif float(i) > 200000 and float(i) < 400000:
        return '40萬內'
    elif float(i) > 400000 and float(i) < 600000:
        return '60萬內'
    else:
        return '60萬以上'
    
def tagAlg(member_event_auction):
    tag_1 = pd.DataFrame(member_event_auction.groupby("BUYERID")['S_NO'].apply(lambda tags: ','.join(tags)))
    tag_2 = pd.DataFrame(member_event_auction.groupby("BUYERID")['CTFC'].apply(lambda tags: ','.join(tags)))
    tag_3 = pd.DataFrame(member_event_auction.groupby("BUYERID")['YM_MFG'].apply(lambda tags: ','.join(tags)))
    tag_4 = pd.DataFrame(member_event_auction.groupby("BUYERID")['里程保證'].apply(lambda tags: ','.join(tags)))
    tag_5 = pd.DataFrame(member_event_auction.groupby("BUYERID")['成交價OR最後出價'].apply(lambda tags: ','.join(tags)))

    tagMemberDic1 = {}
    for index, row in tag_1.iterrows():
        tagMemberDic1[row.name] = None
        tmpList = []
        for j in range(len(row['S_NO'].split(','))):
            tmpList.append(int(row['S_NO'].split(',')[j][1]))
        tagMemberDic1[row.name] = categoryTag(most_frequent(tmpList))
    #         print(row['S_NO'].split(',')[j], row['S_NO'].split(',')[j][1])

    tagMemberDic2 = {}
    for index, row in tag_2.iterrows():
        tagMemberDic2[row.name] = None
        tmpList = []
        for j in range(len(row['CTFC'].split(','))):
            tmpList.append((row['CTFC'].split(',')[j]))
        tagMemberDic2[row.name] = conditionTag(most_frequent(tmpList))

    tagMemberDic3 = {}
    for index, row in tag_3.iterrows():
        tagMemberDic3[row.name] = None
        tmpList = []
        for j in range(len(row['YM_MFG'].split(','))):
            tmpList.append(yearTag(row['YM_MFG'].split(',')[j]))
        tagMemberDic3[row.name] = most_frequent(tmpList)

    tagMemberDic4 = {}
    for index, row in tag_4.iterrows():
        tagMemberDic4[row.name] = None
        tmpList = []
        for j in range(len(row['里程保證'].split(','))):
            tmpList.append(row['里程保證'].split(',')[j])
        tagMemberDic4[row.name] = mileTag(most_frequent(tmpList))

    tagMemberDic5 = {}
    for index, row in tag_5.iterrows():
        tagMemberDic5[row.name] = None
        tmpList = []
        for j in range(len(row['成交價OR最後出價'].split(','))):
            tmpList.append(priceTag(row['成交價OR最後出價'].split(',')[j]))
        tagMemberDic5[row.name] = most_frequent(tmpList)
            
    dictMerge = pd.DataFrame({'Category':pd.Series(tagMemberDic1),'Condition':pd.Series(tagMemberDic2),'Year':pd.Series(tagMemberDic3),'Mile':pd.Series(tagMemberDic4),'Price':pd.Series(tagMemberDic5)})
    
    return dictMerge
        
if __name__ == '__main__':
    print("This is main process")

    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
    startTime_all = time.time()
    sys.path.insert(0, "/home/hadoop/haa_recommend") ## insert at 1, 0 is the script path

    path = '/home/hadoop/haa_recommend'
    os.chdir(path)

    """ 設定 log 的路徑 與層級 """
    logging.basicConfig(filename="/home/hadoop/haa_recommend/logs/Recom.log", level=logging.DEBUG, 
                        format='%(asctime)s %(levelname)s %(name)s %(message)s')
    logger=logging.getLogger(__name__)

    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d")

    getEngine = ConnectionMySql(logger)

    try:

        print(f'{date_time}\nPART 1: 讀取作業成功!')

    except sqlalchemy.exc.OperationalError as err:
        errorLogger(err, logger)
        print('Part 1: 前置作業失敗!')

    # #從資料庫抓到的資料先寫一份檔案暫存
    # try:
    #     getGA_least30Days.to_csv('/home/hadoop/recommendModel/data/last30PVs.csv',encoding='utf-8',index=False,header=True)
    # except Exception as writeFileError:
    #     errorLogger(err, logger)

    #以今日為基準，往前抓 30 日的出價log

    #以今日為基準，往前抓 90 日的拍定車輛紀錄

    #Algorithm
    #1. 以GA資料出發，merge出價log，merge拍定車輛紀錄
    #2. 須將分類轉成數位化的類別
    #3. 先MemberIdGroup = all.groupby(['memberId', 'tagA'])
    #4. MemberIdFrequencyTagA = pd.DataFrame(先MemberIdGroup.size())
    #5. sort by MemberIdFrenquencyTag_+ 出價 ，標籤按照強弱排序
    #6. 依照不同column(tagA)用df.loc分區 => TagA = MemberIdGroup.loc[(MemberIdFrequencyTagA) >=2 or 有出價 ] 
    #7. 再使用all.loc[all.memberId.isin(TagA), 'tagA'].append(0/1/2) => 一個tagA會重複貼標籤，只要符合規則就貼標
    #所有都貼完之後便可以寫檔及寫入DB
    #再規劃是否要撰寫stored procedure or 請後端開api


    try:
        
        
        print(f'{date_time}\nPART 2: 前置作業成功!')

    except Exception as err:
        logger.info(err)
        print('Part 2: 前置作業失敗!')

